#pragma once
#include <stdio.h>
#include <iostream>
#include <conio.h>

using namespace std;

class ABstractFootballTeam
{
protected:
	string teamName;
	string teamManager;
	int numberOfPlayers;
	string teamCity;
public:
	virtual ~ABstractFootballTeam();

	virtual void pobierz(string _teamManager, string _teamName, string _teamCity) = 0;
	virtual void wypisz() = 0;
};

