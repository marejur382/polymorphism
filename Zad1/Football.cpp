#include "stdafx.h"
#include "Football.h"
#include <string>


Football::Football()
{
	numberOfPlayers = 11;
}


Football::~Football()
{
}

void Football::wypisz()
{
	cout << "Football team:" << endl;
	cout << "Team name: " << teamName << endl;
	cout << "Team manager: " << teamManager << endl;
	cout << "Team city: " << teamCity << endl;
	cout << "Number of players: " << numberOfPlayers << endl;
}

void Football::pobierz(string _teamManager, string _teamName, string _teamCity)
{
	teamManager = _teamManager;
	teamName = _teamName;
	teamCity = _teamCity;
}
