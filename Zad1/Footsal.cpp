#include "stdafx.h"
#include "Footsal.h"


Footsal::Footsal()
{
	numberOfPlayers = 6;
}


Footsal::~Footsal()
{
}

void Footsal::wypisz()
{
	cout << "Footsal team:" << endl;
	cout << "Team name: " << teamName << endl;
	cout << "Team manager: " << teamManager << endl;
	cout << "Team city: " << teamCity << endl;
	cout << "Number of players: " << numberOfPlayers << endl;
}

void Footsal::pobierz(string _teamManager, string _teamName, string _teamCity)
{
	teamManager = _teamManager;
	teamName = _teamName;
	teamCity = _teamCity;
}

