// ConsoleApplication1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "Footsal.h"
#include "Football.h"
#include "ABstractFootballTeam.h"
#include <string>
#include <conio.h>

int main()
{
	int menu = 0;
	char teamName[100] = "Legia Warszawa";
	char teamManager[100] = "Marek Borgieł";
	char teamCity[100] = "Warszaw";
	ABstractFootballTeam *newTeam;
	Football football;
	Footsal footsal;

	newTeam = &football;
	newTeam->pobierz(teamName, teamManager, teamCity);
	newTeam->wypisz();
	newTeam = &footsal;
	newTeam->pobierz("Borgłowie", "Marek Borgieł", "Czechowice");
	newTeam->wypisz();
	
	return 0;
}

