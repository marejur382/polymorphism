#include "stdafx.h"
#include "Email.h"


Email::Email()
{
}

Email::~Email()
{
}

// correct email format: abc@abc.com
bool Email::validate(string value)
{
	// regular expression from internet :D
	const regex pattern
	("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	return regex_match(value, pattern);
}
