#pragma once
#include "Validator.h"
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <regex>

class Email :
	public Validator
{
public:
	Email();
	~Email();

	bool validate(string value);
};

