#include "stdafx.h"
#include "PhoneNumber.h"


PhoneNumber::PhoneNumber()
{
}


PhoneNumber::~PhoneNumber()
{
}

// correct phone number format: 123-456-789
bool PhoneNumber::validate(string value)
{
	bool correctPhoneNumber = false;


	char separator = '-';
	for (int i = 0; i < value.length() - 1; i++)
	{
		if (i == 3 || i == 7)
			(value[i] == separator) ? correctPhoneNumber = true : correctPhoneNumber = false;
		else
		{
			correctPhoneNumber = !isalpha(value[i]);
		}
		if (correctPhoneNumber == false)
			break;
	}
	if (value.length() != 11)
		correctPhoneNumber = false;

	return correctPhoneNumber;
}