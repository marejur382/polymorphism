#pragma once
#include "Validator.h"

using namespace std;

class PhoneNumber :
	public Validator
{
public:
	PhoneNumber();
	~PhoneNumber();

	bool validate(string value);
};

