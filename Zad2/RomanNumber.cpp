#include "stdafx.h"
#include "RomanNumber.h"


RomanNumber::RomanNumber()
{
}


RomanNumber::~RomanNumber()
{
}

// correct symbols 
bool RomanNumber::validate(string value)
{
	// regular expression
	const regex pattern
	("^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})");

	return regex_match(value, pattern);
}
