#pragma once
#include "Validator.h"
#include <regex>

class RomanNumber :
	public Validator
{
public:
	RomanNumber();
	~RomanNumber();

	bool validate(string value);
};

