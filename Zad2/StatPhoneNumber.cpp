#include "stdafx.h"
#include "StatPhoneNumber.h"


StatPhoneNumber::StatPhoneNumber()
{
}


StatPhoneNumber::~StatPhoneNumber()
{
}

// correct phone number format: 444-55-66 (pl)
bool StatPhoneNumber::validate(string value)
{
	bool correctStatPhoneNumber = false;


	char separator = '-';
	for (int i = 0; i < value.length() - 1; i++)
	{
		if (i == 3 || i == 6)
			(value[i] == separator) ? correctStatPhoneNumber = true : correctStatPhoneNumber = false;
		else
		{
			correctStatPhoneNumber = !isalpha(value[i]);
		}
		if (correctStatPhoneNumber == false)
			break;
	}
	if (value.length() != 9)
		correctStatPhoneNumber = false;

	return correctStatPhoneNumber;
}