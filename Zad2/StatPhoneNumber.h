#pragma once
#include "Validator.h"

class StatPhoneNumber :
	public Validator
{
public:
	StatPhoneNumber();
	~StatPhoneNumber();

	bool validate(string value);
};

