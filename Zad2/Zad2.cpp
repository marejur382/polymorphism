// Zad2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Validator.h"
#include "PhoneNumber.h"
#include "Value.h"
#include "StatPhoneNumber.h"
#include "Email.h"
#include "RomanNumber.h"
#include <ctype.h>

int main()
{
	Validator *validation;
	Email email;
	PhoneNumber phoneNumber;
	StatPhoneNumber statPhoneNumber;
	Value value;
	RomanNumber romanNumber;

	//good
	validation = &email;
	cout << validation->validate("marekborgiel@gmail.com");
	cout << "\n";
	validation = &phoneNumber;
	cout << validation->validate("123-456-789");
	cout << "\n";
	validation = &statPhoneNumber;
	cout << validation->validate("444-55-66");
	cout << "\n";
	validation = &value;
	cout << validation->validate("123.123");
	cout << "\n";
	validation = &romanNumber;
	cout << validation->validate("VII");
	cout << "\n";

	//wrong
	validation = &email;
	cout << validation->validate("marekborgielgmail.com");
	cout << "\n";
	validation = &phoneNumber;
	cout << validation->validate("123-456-89");
	cout << "\n";
	validation = &statPhoneNumber;
	cout << validation->validate("444-5566");
	cout << "\n";
	validation = &value;
	cout << validation->validate("123.1A");
	cout << "\n";
	validation = &romanNumber;
	cout << validation->validate("VIIA");
	cout << "\n";
	return 0;
}

